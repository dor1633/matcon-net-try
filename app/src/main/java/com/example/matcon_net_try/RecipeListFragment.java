package com.example.matcon_net_try;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.matcon_net_try.model.recipe.Recipe;
import com.example.matcon_net_try.model.recipe.RecipeModel;
import com.squareup.picasso.Picasso;

public class RecipeListFragment extends Fragment {
    RecipeListViewModel viewModel;

    ProgressBar pb;
    Button addBtn;
    MyAdapter adapter;
    SwipeRefreshLayout sref;


    interface Delegate{
        void onItemSelected(String recipeId);
    }

    Delegate parent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("TAG","onCreateView");
        View view = inflater.inflate(R.layout.recipe_list_fragment, container, false);

        viewModel = new ViewModelProvider(this).get(RecipeListViewModel.class);

        ListView list = view.findViewById(R.id.recipeslist_list);
        pb = view.findViewById(R.id.recipeslist_progress);
        addBtn = view.findViewById(R.id.recipeslist_add_btn);
        pb.setVisibility(View.INVISIBLE);
        sref = view.findViewById(R.id.recipeslist_swipe);

        sref.setOnRefreshListener(() -> {
            sref.setRefreshing(true);
            reloadData();
        });

        adapter = new MyAdapter();
        list.setAdapter(adapter);
        list.setOnItemClickListener((adapterView, view1, i, l) -> {
            String id = viewModel.getList().getValue().get(i).getId();
            parent.onItemSelected(id);
        });

        addBtn.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_recipeListFragment_to_addRecipeFragment));

        viewModel.getList().observe(getViewLifecycleOwner(), recipes -> adapter.notifyDataSetChanged());
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Delegate) {
            parent = (Delegate) getActivity();
        } else {
            throw new RuntimeException(context.toString()
                    + "recipe list parent activity must implement recipe list fragment Delegate");
        }
        setHasOptionsMenu(true);

        viewModel = new ViewModelProvider(this).get(RecipeListViewModel.class);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    void reloadData(){
        pb.setVisibility(View.VISIBLE);
        addBtn.setEnabled(false);
        RecipeModel.instance.refreshAllRecipes(() -> {
            pb.setVisibility(View.INVISIBLE);
            addBtn.setEnabled(true);
            sref.setRefreshing(false);
        });
    }

    class MyAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            if (viewModel.getList().getValue() == null){
                return 0;
            }
            return viewModel.getList().getValue().size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null){
                view = getLayoutInflater().inflate(R.layout.list_row, null);
            }
            TextView tv = view.findViewById(R.id.listrow_test_tv);
            ImageView imv = view.findViewById(R.id.listrow_imagev);
            Recipe recipe = viewModel.getList().getValue().get(i);
            tv.setText(recipe.getTitle());

            imv.setImageResource(R.drawable.avatar);
            if (recipe.getImageUrl() != null){
                Picasso.get().load(recipe.getImageUrl()).placeholder(R.drawable.avatar).into(imv);
            }
            return view;
        }
    }
}