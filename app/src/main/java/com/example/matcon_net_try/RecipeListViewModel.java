package com.example.matcon_net_try;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.matcon_net_try.model.recipe.Recipe;
import com.example.matcon_net_try.model.recipe.RecipeModel;

import java.util.List;

public class RecipeListViewModel extends ViewModel {
    private LiveData<List<Recipe>> stList;

    public RecipeListViewModel(){
        Log.d("TAG","RecipeListViewModel");
        stList = RecipeModel.instance.getAllRecipes();
    }
    LiveData<List<Recipe>> getList(){
        return stList;
    }
}