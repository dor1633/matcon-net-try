package com.example.matcon_net_try.model.user;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UserFirebase {
    final static String QUESTION_COLLECTION = "users";

    public static FirebaseUser getCurrentUser(){
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        return firebaseAuth.getCurrentUser();
    }


    public static void getUser(String id, final UserModel.Listener<User> listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(QUESTION_COLLECTION).whereEqualTo("id", id)
                .get().addOnCompleteListener(task -> {
                    User user = null;
                    if (task.isSuccessful()){
                        user = new User();
                        for(QueryDocumentSnapshot doc : task.getResult()){
                            Map<String, Object> json = doc.getData();
                            user = factory(json);
                        }
                    }
                    listener.onComplete(user);
                });
    }


    private static User factory(Map<String, Object> json){
        User us = new User();
        us.id = (String) Objects.requireNonNull(json.get("id"));
        us.username = (String) Objects.requireNonNull(json.get("username"));
        us.name = (String) Objects.requireNonNull(json.get("name"));
        us.email = (String) Objects.requireNonNull(json.get("email"));
        return us;
    }

    public void registerUser(User user, String password, UserModel.RegisterListener listener) {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuth.createUserWithEmailAndPassword(user.email , password)
                .addOnSuccessListener(authResult -> {

                    HashMap<String , Object> map = new HashMap<>();
                    map.put("name" , user.name);
                    map.put("email", user.email);
                    map.put("username" , user.username);
                    map.put("id" , firebaseAuth.getCurrentUser().getUid());
                    map.put("imageurl" , "default");

                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    db.collection("Users").document(firebaseAuth.getCurrentUser().getUid()).set(map).addOnCompleteListener(task -> {
                        if (task.isSuccessful()){
                            listener.onComplete();
                        }
                    });

                }).addOnFailureListener(e -> {
                    listener.onFailed(e.getMessage());
                });
    }

    public void loginUser(String email, String password, UserModel.LoginListener listener) {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuth.signInWithEmailAndPassword(email , password).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                listener.onComplete();
            }

        }).addOnFailureListener(e -> {
            listener.onFailed(e.getMessage());
        });
    }
}
