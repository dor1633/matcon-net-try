package com.example.matcon_net_try.model.recipe;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;

import com.example.matcon_net_try.MyApplication;
import com.example.matcon_net_try.model.AppLocalDb;


import java.util.List;

public class RecipeModel {
    public static final RecipeModel instance = new RecipeModel();

    RecipeFirebase recipeFirebase = new RecipeFirebase();
    RecipeModelSql recipeModelSql = new RecipeModelSql();

    public interface Listener<T> {
        void onComplete(T result);
    }

    LiveData<List<Recipe>> recipeList;
    public LiveData<List<Recipe>> getAllRecipes() {
        if (recipeList == null){
            recipeList = AppLocalDb.db.recipeDao().getAllRecipes();
            refreshAllRecipes(null);
        }
        return recipeList;
    }

    public interface GetAllRecipesListener {
        void onComplete();
    }
    public void refreshAllRecipes(final GetAllRecipesListener listener) {
        //1. get local last update date
        final SharedPreferences sp = MyApplication.context.getSharedPreferences("TAG", Context.MODE_PRIVATE);
        long lastUpdated = sp.getLong("lastUpdated",0);
        //2. get all updated record from firebase from the last update date
        recipeFirebase.getAllRecipes(lastUpdated, new RecipeFirebase.GetAllRecipesListener() {
            @Override
            public void onComplete(List<Recipe> result) {
                //3. insert the new updates to the local db
                long lastU = 0;
                for (Recipe recipe: result) {
                    if(recipe.isDeleted())
                        recipeModelSql.deleteRecipe(recipe,null);
                    else {
                        recipeModelSql.addRecipe(recipe,null);
                        if (recipe.getLastUpdated() > lastU) {
                            lastU = recipe.getLastUpdated();
                        }
                    }
                }
                //4. update the local last update date
                sp.edit().putLong("lastUpdated", lastU).commit();
                //5. return the updates data to the listeners
                if(listener != null){
                    listener.onComplete();
                }
            }
        });
    }

    public interface GetRecipeListener {
        void onComplete(Recipe recipe);
        void onFailure();
    }

    public void getRecipe(String id, GetRecipeListener listener) {
        recipeFirebase.getRecipe(id, listener);
    }

    public interface AddRecipeListener {
        void onComplete();
    }

    public void addRecipe(final Recipe recipe, final AddRecipeListener listener) {
        recipeFirebase.addRecipe(recipe, () -> refreshAllRecipes(listener::onComplete));
    }


    public void updateRecipe(final Recipe recipe, final AddRecipeListener listener) {
        recipeFirebase.updateRecipe(recipe, () -> refreshAllRecipes(listener::onComplete));
    }

    public interface DeleteListener {
        void onComplete();
    }

    public void deleteRecipe(final Recipe recipe, final DeleteListener listener) {
        recipeFirebase.delete(recipe, () -> refreshAllRecipes(listener::onComplete));
    }

    public interface UploadImageListener extends Listener<String>{ }

    public void uploadImage(Bitmap imageBmp, String name, final UploadImageListener listener) {
        recipeFirebase.uploadImage(imageBmp, name, listener);
    }

}
