package com.example.matcon_net_try.model.recipe;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.example.matcon_net_try.model.AppLocalDb;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;

public class RecipeFirebase {

    interface GetAllRecipesListener {
        void onComplete(List<Recipe> list);
    }

    public void getAllRecipes(Long lastUpdated, final GetAllRecipesListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Timestamp ts = new Timestamp(lastUpdated, 0);
        db.collection("recipes").whereGreaterThanOrEqualTo("lastUpdated", ts).get().addOnCompleteListener(task -> {
            List<Recipe> data = new LinkedList<Recipe>();
            if (task.isSuccessful()) {
                for (DocumentSnapshot doc : task.getResult()) {
                    Recipe recipe = new Recipe();
                    recipe.fromMap(doc.getData());
                    data.add(recipe);
                    Log.d("TAG", "st: " + recipe.getId());
                }
            }
            listener.onComplete(data);
        });
    }

    public void addRecipe(Recipe recipe, final RecipeModel.AddRecipeListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        recipe.setUserId(currentUser.getUid());
        db.collection("recipes").document(recipe.getId())
                .set(recipe.toMap()).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "recipe added successfully");
            listener.onComplete();
        }).addOnFailureListener(e -> {
            Log.d("TAG", "fail adding recipe");
            listener.onComplete();
        });
    }


    public void updateRecipe(Recipe recipe, RecipeModel.AddRecipeListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("recipes").document(recipe.getId())
                .update(recipe.toMap()).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "recipe added successfully");
            listener.onComplete();
        }).addOnFailureListener(e -> {
            Log.d("TAG", "fail adding recipe");
            listener.onComplete();
        });
    }

    public void getRecipe(String id, final RecipeModel.GetRecipeListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("recipes").document(id).get().addOnCompleteListener(task -> {
            Recipe recipe = null;
            if (task.isSuccessful()) {
                DocumentSnapshot doc = task.getResult();
                if (doc != null && doc.getData() != null) {
                    recipe = new Recipe();
                    recipe.fromMap(task.getResult().getData());
                } else {
                    listener.onFailure();
                    return;
                }
            }
            listener.onComplete(recipe);
        });
    }

    public void delete(Recipe recipe, final RecipeModel.DeleteListener listener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        recipe.setDeleted(true);
        db.collection("recipes").document(recipe.getId())
                .update(recipe.toMap()).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "recipe delete successfully");
            new Thread(() -> AppLocalDb.db.recipeDao().delete(recipe)).start();
            listener.onComplete();
        }).addOnFailureListener(e -> {
            Log.d("TAG", "fail deleting recipe");
            listener.onComplete();
        });
    }
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//            }
    ;
//    }


    public void uploadImage(Bitmap imageBmp, String name, final RecipeModel.UploadImageListener listener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference imagesRef = storage.getReference().child("images").child(name);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();
        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.onComplete(null);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imagesRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Uri downloadUrl = uri;
                        listener.onComplete(downloadUrl.toString());
                    }
                });
            }
        });
    }
}