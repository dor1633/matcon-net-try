package com.example.matcon_net_try.model;

import androidx.room.Room;

import com.example.matcon_net_try.MyApplication;

public class AppLocalDb {


    public static AppLocalDbRepository db =
            Room.databaseBuilder(MyApplication.context,
                    AppLocalDbRepository.class,
                    "dbFileName.db")
                    .fallbackToDestructiveMigration()
                    .build();

}


