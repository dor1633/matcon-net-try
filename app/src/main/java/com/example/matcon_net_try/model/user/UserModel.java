package com.example.matcon_net_try.model.user;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.matcon_net_try.MyApplication;
import com.example.matcon_net_try.model.AppLocalDb;
import com.google.firebase.auth.FirebaseAuth;


import static android.content.Context.MODE_PRIVATE;

public class UserModel {

    UserFirebase userFirebase = new UserFirebase();

    public static final UserModel instance = new UserModel();

    public interface Listener<T>{
        void onComplete(T data);
    }

    public interface CompListener{
        void onComplete();
    }


    public LiveData<User> getUser(){
        refreshUser(null);
        LiveData<User> liveData = AppLocalDb.db.userDao().getAll();
        return liveData;
    }

    public void refreshUser(final CompListener listener){
        UserFirebase.getUser(FirebaseAuth.getInstance().getCurrentUser().getUid(),new Listener<User>() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onComplete(final User data) {
                new AsyncTask<String, String, String>(){
                    @Override
                    protected String doInBackground(String... strings) {
                        long lastUpdated = 0;
                        AppLocalDb.db.userDao().insertAll(data);

                        SharedPreferences.Editor edit = MyApplication.context.getSharedPreferences("TAG", MODE_PRIVATE).edit();
                        edit.apply();
                        return "";
                    }
                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        if (listener!=null)  listener.onComplete();
                    }
                }.execute("");
            }
        });
    }

    /////////////////////// Auth //////////////////////////////////////////////////////
    public interface RegisterListener {
        void onComplete();
        void onFailed(String err);
    }

    public void registerUser(final User user, String password, final RegisterListener listener) {
        userFirebase.registerUser(user, password, listener);
    }

    public interface LoginListener extends RegisterListener {
    }

    public void loginUser(final String email, String password, final LoginListener listener) {
        userFirebase.loginUser(email, password, listener);
    }
}
