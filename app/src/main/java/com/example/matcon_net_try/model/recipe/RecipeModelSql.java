package com.example.matcon_net_try.model.recipe;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.matcon_net_try.model.AppLocalDb;

import java.util.List;

public class RecipeModelSql {

    public LiveData<List<Recipe>> getAllRecipes(){
        return AppLocalDb.db.recipeDao().getAllRecipes();
    }

    public interface AddRecipeListener{
        void onComplete();
    }
    public void addRecipe(final Recipe recipe, final RecipeModel.AddRecipeListener listener){
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.recipeDao().insertAll(recipe);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null){
                    listener.onComplete();
                }
            }
        };
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }


    public void deleteRecipe(final Recipe recipe, final RecipeModel.AddRecipeListener listener){
        class MyAsyncTask extends AsyncTask {
            @Override
            protected Object doInBackground(Object[] objects) {
                AppLocalDb.db.recipeDao().delete(recipe);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                if (listener != null){
                    listener.onComplete();
                }
            }
        };
        MyAsyncTask task = new MyAsyncTask();
        task.execute();
    }
}
