package com.example.matcon_net_try.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.matcon_net_try.model.recipe.Recipe;
import com.example.matcon_net_try.model.recipe.RecipeDao;
import com.example.matcon_net_try.model.user.User;
import com.example.matcon_net_try.model.user.UserDao;


@Database(entities = { User.class, Recipe.class}, version = 17)
public abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract RecipeDao recipeDao();
}
