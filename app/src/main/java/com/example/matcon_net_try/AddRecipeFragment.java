package com.example.matcon_net_try;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.matcon_net_try.model.recipe.Recipe;
import com.example.matcon_net_try.model.recipe.RecipeModel;
import java.util.UUID;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class AddRecipeFragment extends Fragment {
    ImageView avatarImageView;
    ImageButton editImage;
    EditText titleEt;
    EditText contentEt;
    Button saveBtn;
    Button cancelBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_recipe, container, false);
        avatarImageView = view.findViewById(R.id.addrecipe_avatar_imv);
        editImage = view.findViewById(R.id.addrecipe_edit_image_btn);
        titleEt = view.findViewById(R.id.addrecipe_title_et2);
        contentEt = view.findViewById(R.id.addrecipe_content_et);
        saveBtn = view.findViewById(R.id.addrecipe_save_btn);
        cancelBtn = view.findViewById(R.id.addrecipe_cancel_btn);

        editImage.setOnClickListener(view1 -> editImage());

        saveBtn.setOnClickListener(view12 -> saveRecipe());

        cancelBtn.setOnClickListener(view13 -> Navigation.findNavController(view13).popBackStack());
        return view;
    }

    private void saveRecipe() {
        final Recipe recipe = new Recipe();
        recipe.setTitle(titleEt.getText().toString());
        recipe.setContent(contentEt.getText().toString());

        BitmapDrawable drawable = (BitmapDrawable)avatarImageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        if(recipe.getTitle().equals("") || recipe.getContent().equals("") || drawable == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Operation Failed");
            builder.setMessage("At least one field is empty");
            builder.setNeutralButton("OK", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();
        } else {

            recipe.setId(UUID.randomUUID().toString());

            RecipeModel.instance.uploadImage(bitmap, recipe.getId(), url -> {
                if (url == null) {
                    displayFailedError();
                } else {
                    recipe.setImageUrl(url);
                    RecipeModel.instance.addRecipe(recipe, () -> Navigation.findNavController(saveBtn).popBackStack());
                }
            });
        }
    }

    public void displayFailedError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Operation Failed");
        builder.setMessage("Saving image failed, please try again later...");
        builder.setNeutralButton("OK", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    private void editImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose your profile picture");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        avatarImageView.setImageBitmap(selectedImage);
                    }
                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage =  data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                avatarImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                cursor.close();
                            }
                        }
                    }
                    break;
            }
        }
    }
}