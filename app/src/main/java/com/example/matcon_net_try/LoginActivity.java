package com.example.matcon_net_try;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.matcon_net_try.model.user.UserModel;

public class LoginActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private Button loginBtn;
    private TextView registerUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        loginBtn = findViewById(R.id.login);
        registerUser = findViewById(R.id.register_user);

        registerUser.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this , RegisterActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)));

        loginBtn.setOnClickListener(v -> {
            String txt_email = email.getText().toString();
            String txt_password = password.getText().toString();

            if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
                Toast.makeText(LoginActivity.this, "Empty Credentials!", Toast.LENGTH_SHORT).show();
            } else {
                loginUser(txt_email , txt_password);
            }
        });
    }

    private void loginUser(String email, String password) {

        UserModel.instance.loginUser(email, password, new UserModel.LoginListener() {
            @Override
            public void onComplete() {
                Toast.makeText(LoginActivity.this, "Welcome for Matconet", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this , HomePageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailed(String err) {
                Toast.makeText(LoginActivity.this, err, Toast.LENGTH_SHORT).show();
            }
        });
    }
}