package com.example.matcon_net_try;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.matcon_net_try.model.user.User;
import com.example.matcon_net_try.model.user.UserModel;

public class RegisterActivity extends AppCompatActivity {

    private EditText username;
    private EditText name;
    private EditText email;
    private EditText password;
    private Button registerBtn;
    private TextView loginUser;


    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        username = findViewById(R.id.username);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        registerBtn = findViewById(R.id.register);
        loginUser = findViewById(R.id.login_user);

        pd = new ProgressDialog(this);

        loginUser.setOnClickListener(v -> startActivity(new Intent(RegisterActivity.this, LoginActivity.class)));


        registerBtn.setOnClickListener(v -> {
            String txtUsername = username.getText().toString();
            String txtName = name.getText().toString();
            String txtEmail = email.getText().toString();
            String txtPassword = password.getText().toString();

            if (TextUtils.isEmpty(txtUsername) || TextUtils.isEmpty(txtName)
                    || TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPassword)){
                Toast.makeText(RegisterActivity.this, "Empty credentials!", Toast.LENGTH_SHORT).show();
            } else if (txtPassword.length() < 6){
                Toast.makeText(RegisterActivity.this, "Password too short!", Toast.LENGTH_SHORT).show();
            } else {
                registerUser(txtUsername , txtName , txtEmail , txtPassword);
            }
        });
    }

        private void registerUser(final String username, final String name, final String email, String password) {

            pd.setMessage("Please wait!");
            pd.show();

            User user = new User("", username, name, email);
            UserModel.instance.registerUser(user, password, new UserModel.RegisterListener() {
                @Override
                public void onComplete() {
                    pd.dismiss();
                    Intent intent = new Intent(RegisterActivity.this , HomePageActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onFailed(String err) {
                    Log.e("E", err);
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, err, Toast.LENGTH_SHORT).show();
                }
            });
    }
}