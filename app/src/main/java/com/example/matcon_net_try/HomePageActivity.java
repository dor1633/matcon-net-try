package com.example.matcon_net_try;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

public class HomePageActivity extends AppCompatActivity implements RecipeListFragment.Delegate{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }

    @Override
    public void onItemSelected(String recipeId) {
        RecipeListFragmentDirections.ActionRecipeListFragmentToRecipeDetailsFragment direc = RecipeListFragmentDirections.actionRecipeListFragmentToRecipeDetailsFragment(recipeId);
        Navigation.findNavController(this, R.id.home_nav_host).navigate(direc);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}