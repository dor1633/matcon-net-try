package com.example.matcon_net_try;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.navigation.Navigation;

import com.example.matcon_net_try.model.recipe.Recipe;
import com.example.matcon_net_try.model.recipe.RecipeModel;
import com.example.matcon_net_try.model.user.UserFirebase;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;


public class RecipeDetailsFragment extends AddRecipeFragment {
    Recipe recipe;
    Button deleteRecipe;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        // setup all recipe info in the display
        editImage.setVisibility(View.INVISIBLE);
        titleEt.setEnabled(false);
        contentEt.setEnabled(false);
        saveBtn.setVisibility(View.INVISIBLE);
        cancelBtn.setVisibility(view.INVISIBLE);

        deleteRecipe = view.findViewById(R.id.delete_recipe_btn);


        deleteRecipe.setOnClickListener(view14 -> deleteRecipe());

        final String recipeId = RecipeDetailsFragmentArgs.fromBundle(getArguments()).getRecipeId();
        Log.d("recipeId:", recipeId);

        RecipeModel.instance.getRecipe(recipeId, new RecipeModel.GetRecipeListener() {
            @Override
            public void onComplete(Recipe rc) {
                recipe = rc;
                titleEt.setText(recipe.getTitle());
                contentEt.setText(recipe.getContent());
                if (recipe.getImageUrl() != null){
                    Picasso.get().load(recipe.getImageUrl()).placeholder(R.drawable.avatar).into(avatarImageView);
                }

                FirebaseUser currentUser = UserFirebase.getCurrentUser();
                if(currentUser != null) {
                    String userId = currentUser.getUid();
                    if(userId.equals(recipe.getUserId())) {
                        titleEt.setEnabled(true);
                        contentEt.setEnabled(true);

                        editImage.setVisibility(View.VISIBLE);
                        saveBtn.setVisibility(View.VISIBLE);

                        // Edit button
                        saveBtn.setText("Edit");
                        saveBtn.setTextColor(Color.BLACK);
                        saveBtn.setBackgroundColor(Color.YELLOW);
                        saveBtn.setOnClickListener(view1 -> editRecipe());

                        deleteRecipe.setVisibility(View.VISIBLE);
                        cancelBtn.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure() {
                Toast.makeText(getContext(), "Recipe is not exist!!!!!!", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }




    private void editRecipe() {
        recipe.setTitle(titleEt.getText().toString());
        recipe.setContent(contentEt.getText().toString());
        recipe.setLastUpdated(Timestamp.now().getSeconds());

        BitmapDrawable drawable = (BitmapDrawable)avatarImageView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        RecipeModel.instance.uploadImage(bitmap, recipe.getId(), url -> {
            if (url == null) {
                displayFailedError();
            } else {
                recipe.setImageUrl(url);
                RecipeModel.instance.updateRecipe(recipe, () -> Navigation.findNavController(saveBtn).popBackStack());
            }
        });
    }

    private void deleteRecipe() {
        Log.d("deleteeeeeeeeeeeeee", "deleteee");
        RecipeModel.instance.deleteRecipe(recipe, () -> Navigation.findNavController(deleteRecipe).popBackStack());
    }
}